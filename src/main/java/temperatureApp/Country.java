package temperatureApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Country {
    private String name;
    private List<City> citiesList;
    private int size;

    public Country(String name) {
        this.citiesList = new LinkedList<>();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCitiesList() {
        return citiesList;
    }

    public int getSize() {
        return size;
    }

    public void addCity(City city) {
        this.citiesList.add(city);
        this.size++;
    }

    public City getCity(String name) {
        for (City c : citiesList) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public void printCities(List<City> cityList) {
        for (City c : cityList) {
            System.out.println(c.getName());
        }
    }

    /*public List<City> highestTemp10Cities(String strDate) throws ParseException {
        Map<String, Double> cities = new HashMap<String, Double>();

        for (City c : this.citiesList) {
            cities.put(c.getName(),  c.getDayHighestTemp(strDate));
        }

        return ;
    }*/

    public static Date convertToDate(String date) throws ParseException {//
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(date);
    }


}

