package temperatureApp;

import java.util.Date;

public class Temperature {
    private Double celsius;
    private Date date;

    public Temperature(Double celsius) {
        this.celsius = celsius;
        this.date = new Date();
    }

    public Double getCelsius() {
        return celsius;
    }

    public void setCelsius(Double celsius) {
        this.celsius = celsius;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



    /*public static void main(String[] args) {
        Date d = new Date();

        Instant now = Instant.now();
        Instant nowMinus10 = now.minus(Duration.ofDays(10));

        Date dateNow = Date.from(now);
        Date dateNowMinus10 = Date.from(nowMinus10);

        d.after(dateNowMinus10);
    }*/
}
