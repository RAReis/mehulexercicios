package temperatureApp;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class City {
    private String name;
    private Country country;
    private double latitude;
    private double longitude;
    private List<Temperature> temperature;


    public City(String name, Country country, double latitude, double longitude, Temperature temperature) {
        this.name = name;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.temperature = new LinkedList<>();
        this.temperature.add(temperature);
        this.country.addCity(this);
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public List<Temperature> getTemperature() {
        return temperature;
    }

    public Country getCountry() {
        return country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setTemperature(List<Temperature> temperature) {
        this.temperature = temperature;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void addTemperature(Temperature temp) {
        this.temperature.add(temp);
    }

    public Double getXDaysHighestTemp(int x) {
        LinkedList<Double> tempListXDays = new LinkedList<>();
        Double highestTemp = Double.MIN_VALUE;

        Instant now = Instant.now();
        Instant nowMinusX = now.minus(Duration.ofDays(x));

        Date dateNow = Date.from(now);
        Date dateNowMinusX = Date.from(nowMinusX);

        for (Temperature e : this.temperature) {
            if (dateNowMinusX.compareTo(e.getDate()) == -1) {
                tempListXDays.add(e.getCelsius());
            }
        }

        for (Double d : tempListXDays) {
            if (d > highestTemp) {
                highestTemp = d;
            }
        }
        return highestTemp;
    }

    public Double getDayHighestTemp(String strDate) throws ParseException {
        boolean change = false;
        Double highestTemp = Double.MIN_VALUE;
        Date date = convertToDate(strDate);

        for (Temperature d: this.temperature) {
            if(compareDates(date, d.getDate()) == 0){
                if(d.getCelsius() > highestTemp){
                    change=true;
                    highestTemp = d.getCelsius();
                }
            }
        }

        if(change){
            return highestTemp;
        }
        return null;
    }

    private static final ThreadLocal<DateFormat> dateFormatThreadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyyMMdd"));

    public static int compareDates(Date date1, Date date2) {
        DateFormat dateFormat = dateFormatThreadLocal.get();
        return dateFormat.format(date1).compareTo(dateFormat.format(date2));
    }

    public static Date convertToDate(String date) throws ParseException {//
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(date);
    }
}

