package temperatureApp;

import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException {

        Country pt = new Country("Portugal");
        Temperature temp = new Temperature(-4.0);

        City c1 = new City("Chaves", pt, 41.7411, 7.4706, temp);
        City c2 = new City("Porto", pt, 41.1579, 8.6291, new Temperature(10.0));
        City c3 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(3.0));
        /*City c4 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(4.0));
        City c5 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(5.0));
        City c6 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(6.0));
        City c7 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(7.0));
        City c8 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(8.0));
        City c9 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(9.0));
        City c10 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(10.0));
        City c11 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(11.0));
        City c12 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(12.0));
        City c13 = new City("Lisboa", pt, 38.7223, 9.1393, new Temperature(13.0));*/

        c1.addTemperature(new Temperature(1.0));
        c1.addTemperature(new Temperature(20.0));
        c1.addTemperature(new Temperature(-3.0));
        c1.addTemperature(new Temperature(18.0));

        //System.out.println("Highest temp in last X days: " + c1.getXDaysHighestTemp(1) + "Cº");

        c2.addTemperature(new Temperature(1.0));
        c2.addTemperature(new Temperature(12.0));
        c2.addTemperature(new Temperature(2.0));
        c2.addTemperature(new Temperature(3.0));

        System.out.println("Highest temp in City2: " + c2.getDayHighestTemp("2019-02-14") + " Cº");

        //System.out.println(c3.getXDaysHighestTemp(1));
        pt.printCities(pt.getCitiesList());


    }


}
